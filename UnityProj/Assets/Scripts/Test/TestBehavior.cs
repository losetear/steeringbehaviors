﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace BlackCoffee
{
    class TestBehavior : MovingEntity
    {
        public static List<TestBehavior> AllMembers = new List<TestBehavior>();

        SteeringBehaviors steer;

        internal SteeringBehaviors Steering() { return steer; }

        public TestBehavior(Vector2 position,
                               float radius,
                               Vector2 velocity,
                               float max_speed,
                               Vector2 heading,
                               float mass,
                               Vector2 scale,
                               float turn_rate,
                               float max_force):base(position,radius,velocity,max_speed,heading,mass,scale,turn_rate,max_force)
        {
            
        }

        internal override void Update()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        internal override void Render()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        internal override bool HandleMessage(Telegram msg)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}