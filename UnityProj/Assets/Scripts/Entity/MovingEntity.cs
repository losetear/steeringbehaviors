﻿using UnityEngine;
using System.Collections;
using System;

namespace BlackCoffee
{
    internal class MovingEntity : BaseGameEntity
    {
        protected Vector2 m_vVelocity;

        //a normalized vector pointing in the direction the entity is heading. 
        protected Vector2 m_vHeading;

        //a vector perpendicular to the heading vector
        protected Vector2 m_vSide;

        protected float m_dMass;

        //the maximum speed this entity may travel at.
        protected float m_dMaxSpeed;

        //the maximum force this entity can produce to power itself 
        //(think rockets and thrust)
        protected float m_dMaxForce;

        //the maximum rate (radians per second)this vehicle can rotate         
        protected float m_dMaxTurnRate;

        internal MovingEntity(Vector2 position,
                               float radius,
                               Vector2 velocity,
                               float max_speed,
                               Vector2 heading,
                               float mass,
                               Vector2 scale,
                               float turn_rate,
                               float max_force)
            : base(BaseGameEntity.GetNextValidID())
        {
            m_vHeading = heading;
            m_vVelocity = velocity;
            m_dMass = mass;
            m_vSide = Utility.Vector2Perp(m_vHeading);
            m_dMaxSpeed = max_speed;
            m_dMaxTurnRate = turn_rate;
            m_dMaxForce = max_force;
            m_vPosition = position;
            m_dBoundingRadius = radius;
            m_vScale = scale;
        }

        //accessors
        internal Vector2 Velocity() { return m_vVelocity; }
        internal void SetVelocity(Vector2 NewVel) { m_vVelocity = NewVel; }

        internal double Mass() { return m_dMass; }

        internal Vector2 Side() { return m_vSide; }

        internal float MaxSpeed() { return m_dMaxSpeed; }
        internal void SetMaxSpeed(float new_speed) { m_dMaxSpeed = new_speed; }

        internal float MaxForce() { return m_dMaxForce; }
        internal void SetMaxForce(float mf) { m_dMaxForce = mf; }

        internal bool IsSpeedMaxedOut()
        {
            return m_dMaxSpeed * m_dMaxSpeed >= m_vVelocity.sqrMagnitude;
        }

        internal float Speed() { return m_vVelocity.magnitude; }
        internal double SpeedSq() { return m_vVelocity.sqrMagnitude; }

        internal Vector2 Heading() { return m_vHeading; }

        //------------------------- SetHeading ----------------------------------------
        //
        //  first checks that the given heading is not a vector of zero length. If the
        //  new heading is valid this fumction sets the entity's heading and side 
        //  vectors accordingly
        //-----------------------------------------------------------------------------
        internal void SetHeading(Vector2 new_heading)
        {
            Utility.Assert(new_heading.sqrMagnitude> 0.00001f);

            m_vHeading = new_heading;

            //the side vector must always be perpendicular to the heading
            m_vSide = Utility.Vector2Perp(m_vHeading);

        }

        //--------------------------- RotateHeadingToFacePosition ---------------------
        //
        //  given a target position, this method rotates the entity's heading and
        //  side vectors by an amount not greater than m_dMaxTurnRate until it
        //  directly faces the target.
        //
        //  returns true when the heading is facing in the desired direction
        //-----------------------------------------------------------------------------
        internal bool RotateHeadingToFacePosition(Vector2 target)
        {
            Vector2 toTarget = (target - m_vPosition).normalized;

            float dot = Vector2.Dot(m_vHeading,toTarget);

            //some compilers lose acurracy so the value is clamped to ensure it
            //remains valid for the acos
            Mathf.Clamp(dot, -1, 1);

            //first determine the angle between the heading vector and the target
            float angle = Mathf.Acos(dot);

            //return true if the player is facing the target
            if (angle < 0.00001) return true;

            //clamp the amount to turn to the max turn rate
            if (angle > m_dMaxTurnRate) angle = m_dMaxTurnRate;

            //throw new NotImplementedException("RotateHeadingToFacePosition");
            //The next few lines use a rotation matrix to rotate the player's heading
            //vector accordingly
            Utility.RotateUp(ref m_vHeading,angle);
            m_vSide = Utility.Vector2Perp(m_vHeading);
            Utility.RotateUp(ref m_vVelocity, angle);

            return false;
        }

        internal float MaxTurnRate() { return m_dMaxTurnRate; }
        internal void SetMaxTurnRate(float val) { m_dMaxTurnRate = val; }
    }
}
