﻿using UnityEngine;
using System.Collections;
using System;

namespace BlackCoffee
{
    internal class BaseGameEntity
    {
        //////////////////////////////////////////////////////////////////////////private
        //each entity has a unique ID
        int m_ID;

        //every entity has a type associated with it (health, troll, ammo etc)
        int m_iType;

        //this is a generic flag. 
        bool m_bTag;

        //this is the next valid ID. Each time a BaseGameEntity is instantiated
        //this value is updated
        static int m_iNextValidID = 0;

        //this must be called within each constructor to make sure the ID is set
        //correctly. It verifies that the value passed to the method is greater
        //or equal to the next valid ID, before setting the ID and incrementing
        //the next valid ID
        void SetID(int val)
        {
            //make sure the val is equal to or greater than the next available ID
            Utility.Assert((val >= m_iNextValidID), "<BaseGameEntity::SetID>: invalid ID");

            m_ID = val;

            m_iNextValidID = m_ID + 1;
        }

        short default_entity_type = -1;

        //////////////////////////////////////////////////////////////////////////protect
        //its location in the environment
        protected Vector2 m_vPosition = Vector2.zero;

        protected Vector2 m_vScale = Vector2.one;

        //the magnitude of this object's bounding radius
        protected double m_dBoundingRadius;
        protected BaseGameEntity(int ID)
        {
            m_dBoundingRadius = 0.0;
            m_vScale = new Vector2(1.0f, 1.0f);
            m_iType = default_entity_type;
            m_bTag = false;

            SetID(ID);
        }

        //////////////////////////////////////////////////////////////////////////public
        //~BaseGameEntity(){}

        internal virtual void Update() { }

        internal virtual void Render() { }

        internal virtual bool HandleMessage(Telegram msg) { return false; }

        //entities should be able to read/write their data to a stream
        //internal virtual void Write(std::ostream&  os)const{}
        //internal virtual void Read (std::ifstream& is){}

        //use this to grab the next valid ID
        internal static int GetNextValidID() { return m_iNextValidID; }

        //this can be used to reset the next ID
        internal static void ResetNextValidID() { m_iNextValidID = 0; }



        internal Vector2 Pos() { return m_vPosition; }
        internal void SetPos(Vector2 new_pos) { m_vPosition = new_pos; }

        internal float BRadius() { return (float)m_dBoundingRadius; }
        internal void SetBRadius(double r) { m_dBoundingRadius = r; }
        internal int ID() { return m_ID; }

        internal bool IsTagged() { return m_bTag; }
        internal void Tag() { m_bTag = true; }
        internal void UnTag() { m_bTag = false; }

        internal Vector2 Scale() { return m_vScale; }
        internal void SetScale(Vector2 val) { m_dBoundingRadius *= Math.Max(val.x, val.y) / Math.Max(m_vScale.x, m_vScale.y); m_vScale = val; }
        internal void SetScale(float val) { m_dBoundingRadius *= (val / Math.Max(m_vScale.x, m_vScale.y)); m_vScale = new Vector2(val, val); }

        internal int EntityType() { return m_iType; }
        internal void SetEntityType(int new_type) { m_iType = new_type; }
    }
}
