﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BlackCoffee
{
    public class EntityManager
    {
        //internal   static EntityManager Instance();
        internal static readonly EntityManager Instance = new EntityManager();

        //to facilitate quick lookup the entities are stored in a std::map, in which
        //pointers to entities are cross referenced by their identifying number
        Dictionary<int, BaseGameEntity> m_EntityMap;

        EntityManager()
        {
            m_EntityMap = new Dictionary<int, BaseGameEntity>();
        }

        //copy ctor and assignment should be private
        //EntityManager(const EntityManager&);
        //EntityManager& operator=(const EntityManager&);

        //this method stores a pointer to the entity in the std::vector
        //m_Entities at the index position indicated by the entity's ID
        //(makes for faster access)
        internal void RegisterEntity(BaseGameEntity NewEntity)
        {
            m_EntityMap.Add(NewEntity.ID(), NewEntity);
        }

        //returns a pointer to the entity with the ID given as a parameter
        internal BaseGameEntity GetEntityFromID(int id)
        {
            BaseGameEntity res = null;
            if (!m_EntityMap.TryGetValue(id, out res))
            {
                Utility.Assert(false, "<EntityManager::GetEntityFromID>: invalid ID");
            }
            return res;
        }

        //this method removes the entity from the list
        internal void RemoveEntity(BaseGameEntity pEntity)
        {
            m_EntityMap.Remove(pEntity.ID());
        }

        //clears all entities from the entity map
        internal void Reset() { m_EntityMap.Clear(); }
    }
}
