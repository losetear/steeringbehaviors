﻿using System.Collections.Generic;

namespace BlackCoffee
{
    public class MessageDispatcher
    {
        //a std::set is used as the container for the delayed messages
        //because of the benefit of automatic sorting and avoidance
        //of duplicates. Messages are sorted by their dispatch time.
        HashSet<Telegram> PriorityQ;
        //this method is utilized by DispatchMsg or DispatchDelayedMessages.
        //This method calls the message handling member function of the receiving
        //entity, pReceiver, with the newly created telegram
        void Discharge(BaseGameEntity pReceiver, Telegram telegram)
        {
            if (!pReceiver.HandleMessage(telegram))
            {
                //telegram could not be handled
#if SHOW_MESSAGING_INFO
            Utility.Assert(false, "Message not handled");
#endif
            }
        }

        MessageDispatcher() {
            PriorityQ = new HashSet<Telegram>();
        }

        //copy ctor and assignment should be private
        MessageDispatcher(MessageDispatcher msg) { }
        //MessageDispatcher operator=(MessageDispatcher){}

        //////////////////////////////////////////////////////////////////////////public
        internal static readonly MessageDispatcher Instance = new MessageDispatcher();
        //static MessageDispatcher* Instance();

        //send a message to another agent. Receiving agent is referenced by ID.
        internal void DispatchMsg(double delay,
                         int sender,
                         int receiver,
                         int msg,
                         object AdditionalInfo)
        {
            //get a pointer to the receiver
            BaseGameEntity pReceiver = EntityManager.Instance.GetEntityFromID(receiver);

            //make sure the receiver is valid
            if (pReceiver == null)
            {
#if SHOW_MESSAGING_INFO
    Utility.Assert(false,"\nWarning! No Receiver with ID of " + receiver.ToString() + " found");
#endif

                return;
            }

            //create the telegram
            Telegram telegram = new Telegram(0, sender, receiver, msg, AdditionalInfo);

            //if there is no delay, route telegram immediately                       
            if (delay <= 0.0)
            {
#if SHOW_MESSAGING_INFO
    Utility.Assert(false, "\nTelegram dispatched at time: " + TickCounter->GetCurrentFrame()
         + " by " + sender + " for " + receiver 
         + ". Msg is " + msg + "");
#endif

                //send the telegram to the recipient
                Discharge(pReceiver, telegram);
            }
            //else calculate the time when the telegram should be dispatched
            else
            {
                double CurrentTime = FrameCounter.Instance.GetCurrentFrame(); 

                telegram.DispatchTime = CurrentTime + delay;

                //and put it in the queue
                PriorityQ.Add(telegram);

#if SHOW_MESSAGING_INFO
    Utility.Assert(false,  "\nDelayed telegram from " + sender + " recorded at time " 
            + TickCounter->GetCurrentFrame() + " for " + receiver
            + ". Msg is " + msg + "");
#endif
            }
        }

        //send out any delayed messages. This method is called each time through   
        //the main game loop.
        internal void DispatchDelayedMessages()
        {
        }
    }
}
