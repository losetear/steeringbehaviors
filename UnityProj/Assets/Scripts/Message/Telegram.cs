﻿//------------------------------------------------------------------------
//
//  Name:   Telegram.h
//
//  Desc:   This defines a telegram. A telegram is a data structure that
//          records information required to dispatch messages. Messages 
//          are used by game agents to communicate with each other.
//
//  Author: Mat Buckland (fup@ai-junkie.com)
//
//------------------------------------------------------------------------

namespace BlackCoffee
{
    public class Telegram
    {
        //the entity that sent this telegram
        internal int Sender;

        //the entity that is to receive this telegram
        internal int Receiver;

        //the message itself. These are all enumerated in the file
        //"MessageTypes.h"
        internal int Msg;

        //messages can be dispatched immediately or delayed for a specified amount
        //of time. If a delay is necessary this field is stamped with the time 
        //the message should be dispatched.
        internal double DispatchTime;

        //any additional information that may accompany the message
        internal object ExtraInfo;


        internal Telegram()
        {
            DispatchTime = -1;
            Sender = -1;
            Receiver = -1;
            Msg = -1;
        }


        internal Telegram(double time,
                 int sender,
                 int receiver,
                 int msg,
                 object info)
        {
            DispatchTime = time;
            Sender = sender;
            Receiver = receiver;
            Msg = msg;
            ExtraInfo = info;
        }
    }
}