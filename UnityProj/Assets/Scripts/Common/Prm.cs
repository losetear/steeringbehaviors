﻿using UnityEngine;
using System.Collections;
using IniParser;
using IniParser.Model;
using System;
using BlackCoffee;

public class Prm {

#region 加载数据
    static IniData parsedData;

    public static void LoadFromContent(string content)
    {
        FileIniDataParser fileIniData = new FileIniDataParser();

        //Parse the ini file
        parsedData = fileIniData.ReadContent(content);

        //////////////////////////////////////////////////////////////////////////
        GoalWidth = GetFloat("GoalWidth");

        NumSupportSpotsX = GetInt("NumSweetSpotsX");
        NumSupportSpotsY = GetInt("NumSweetSpotsY");

        Spot_PassSafeScore = GetFloat("Spot_CanPassScore");
        Spot_CanScoreFromPositionScore = GetFloat("Spot_CanScoreFromPositionScore");
        Spot_DistFromControllingPlayerScore = GetFloat("Spot_DistFromControllingPlayerScore");
        Spot_ClosenessToSupportingPlayerScore = GetFloat("Spot_ClosenessToSupportingPlayerScore");
        Spot_AheadOfAttackerScore = GetFloat("Spot_AheadOfAttackerScore");

        SupportSpotUpdateFreq = GetFloat("SupportSpotUpdateFreq");

        ChancePlayerAttemptsPotShot = GetFloat("ChancePlayerAttemptsPotShot");
        ChanceOfUsingArriveTypeReceiveBehavior = GetFloat("ChanceOfUsingArriveTypeReceiveBehavior");

        BallSize = GetFloat("BallSize");
        BallMass = GetFloat("BallMass");
        Friction = GetFloat("Friction");

        KeeperInBallRange = GetFloat("KeeperInBallRange");
        PlayerInTargetRange = GetFloat("PlayerInTargetRange");
        PlayerKickingDistance = GetFloat("PlayerKickingDistance");
        PlayerKickFrequency = GetFloat("PlayerKickFrequency");


        PlayerMass = GetFloat("PlayerMass");
        PlayerMaxForce = GetFloat("PlayerMaxForce");
        PlayerMaxSpeedWithBall = GetFloat("PlayerMaxSpeedWithBall");
        PlayerMaxSpeedWithoutBall = GetFloat("PlayerMaxSpeedWithoutBall");
        PlayerMaxTurnRate = GetFloat("PlayerMaxTurnRate");
        PlayerScale = GetFloat("PlayerScale");
        PlayerComfortZone = GetFloat("PlayerComfortZone");
        PlayerKickingAccuracy = GetFloat("PlayerKickingAccuracy");

        NumAttemptsToFindValidStrike = GetInt("NumAttemptsToFindValidStrike");



        MaxDribbleForce = GetFloat("MaxDribbleForce");
        MaxShootingForce = GetFloat("MaxShootingForce");
        MaxPassingForce = GetFloat("MaxPassingForce");

        WithinRangeOfHome = GetFloat("WithinRangeOfHome");
        WithinRangeOfSupportSpot = GetFloat("WithinRangeOfSweetSpot");

        MinPassDist = GetFloat("MinPassDistance");
        GoalkeeperMinPassDist = GetFloat("GoalkeeperMinPassDistance");

        GoalKeeperTendingDistance = GetFloat("GoalKeeperTendingDistance");
        GoalKeeperInterceptRange = GetFloat("GoalKeeperInterceptRange");
        BallWithinReceivingRange = GetFloat("BallWithinReceivingRange");

        bStates = GetBool("ViewStates");
        bIDs = GetBool("ViewIDs");
        bSupportSpots = GetBool("ViewSupportSpots");
        bRegions = GetBool("ViewRegions");
        bShowControllingTeam = GetBool("bShowControllingTeam");
        bViewTargets = GetBool("ViewTargets");
        bHighlightIfThreatened = GetBool("HighlightIfThreatened");

        FrameRate = GetInt("FrameRate");

        SeparationCoefficient = GetFloat("SeparationCoefficient");
        ViewDistance = GetFloat("ViewDistance");
        bNonPenetrationConstraint = GetBool("bNonPenetrationConstraint");


        BallWithinReceivingRangeSq = BallWithinReceivingRange * BallWithinReceivingRange;
        KeeperInBallRangeSq = KeeperInBallRange * KeeperInBallRange;
        PlayerInTargetRangeSq = PlayerInTargetRange * PlayerInTargetRange;
        PlayerKickingDistance += BallSize;
        PlayerKickingDistanceSq = PlayerKickingDistance * PlayerKickingDistance;
        PlayerComfortZoneSq = PlayerComfortZone * PlayerComfortZone;
        GoalKeeperInterceptRangeSq = GoalKeeperInterceptRange * GoalKeeperInterceptRange;
        WithinRangeOfSupportSpotSq = WithinRangeOfSupportSpot * WithinRangeOfSupportSpot;

        //////////////////////////////////////////////////////////////////////////
        //release
        parsedData = null;
    }

    public static float GetFloat(string keyName)
    {
        Utility.Assert(parsedData != null, "LoadFromContent: parsedData is null.");

        return Convert.ToSingle(parsedData["Common"][keyName]);
    }

    public static int GetInt(string keyName)
    {
        Utility.Assert(parsedData != null, "LoadFromContent: parsedData is null.");

        return Convert.ToInt32(parsedData["Common"][keyName]);
    }

    public static bool GetBool(string keyName)
    {
        Utility.Assert(parsedData != null, "LoadFromContent: parsedData is null.");

        return Convert.ToBoolean(parsedData["Common"][keyName] == "1");
    }
#endregion

    public static float GoalWidth;

    public static int NumSupportSpotsX;
    public static int NumSupportSpotsY;

    //these values tweak the various rules used to calculate the support spots
    public static float Spot_PassSafeScore;
    public static float Spot_CanScoreFromPositionScore;
    public static float Spot_DistFromControllingPlayerScore;
    public static float Spot_ClosenessToSupportingPlayerScore;
    public static float Spot_AheadOfAttackerScore;

    public static float SupportSpotUpdateFreq;

    public static float ChancePlayerAttemptsPotShot;
    public static float ChanceOfUsingArriveTypeReceiveBehavior;

    public static float BallSize;
    public static float BallMass;
    public static float Friction;

    public static float KeeperInBallRange;
    public static float KeeperInBallRangeSq;

    public static float PlayerInTargetRange;
    public static float PlayerInTargetRangeSq;

    public static float PlayerMass;

    //max steering force
    public static float PlayerMaxForce;
    public static float PlayerMaxSpeedWithBall;
    public static float PlayerMaxSpeedWithoutBall;
    public static float PlayerMaxTurnRate;
    public static float PlayerScale;
    public static float PlayerComfortZone;

    public static float PlayerKickingDistance;
    public static float PlayerKickingDistanceSq;

    public static float PlayerKickFrequency;

    public static float MaxDribbleForce;
    public static float MaxShootingForce;
    public static float MaxPassingForce;

    public static float PlayerComfortZoneSq;

    //in the range zero to 1.0. adjusts the amount of noise added to a kick,
    //the lower the value the worse the players get
    public static float PlayerKickingAccuracy;

    //the number of times the SoccerTeam::CanShoot method attempts to find
    //a valid shot
    public static int NumAttemptsToFindValidStrike;

    //the distance away from the center of its home region a player
    //must be to be considered at home
    public static float WithinRangeOfHome;

    //how close a player must get to a sweet spot before he can change state
    public static float WithinRangeOfSupportSpot;
    public static float WithinRangeOfSupportSpotSq;


    //the minimum distance a receiving player must be from the passing player
    public static float MinPassDist;
    public static float GoalkeeperMinPassDist;

    //this is the distance the keeper puts between the back of the net
    //and the ball when using the interpose steering behavior
    public static float GoalKeeperTendingDistance;

    //when the ball becomes within this distance of the goalkeeper he
    //changes state to intercept the ball
    public static float GoalKeeperInterceptRange;
    public static float GoalKeeperInterceptRangeSq;

    //how close the ball must be to a receiver before he starts chasing it
    public static float BallWithinReceivingRange;
    public static float BallWithinReceivingRangeSq;


    //these values control what debug info you can see
    public static bool bStates;
    public static bool bIDs;
    public static bool bSupportSpots;
    public static bool bRegions;
    public static bool bShowControllingTeam;
    public static bool bViewTargets;
    public static bool bHighlightIfThreatened;

    public static int FrameRate;


    public static float SeparationCoefficient;

    //how close a neighbour must be before an agent perceives it
    public static float ViewDistance;

    //zero this to turn the constraint off
    public static bool bNonPenetrationConstraint;
}
