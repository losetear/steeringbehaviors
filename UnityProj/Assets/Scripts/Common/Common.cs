﻿using UnityEngine;
using System.Collections;

public class Common
{
    //to make code easier to read
    public const float SEND_MSG_IMMEDIATELY = 0.0f;
    public const int NO_ADDITIONAL_INFO = 0;
    public const int SENDER_ID_IRRELEVANT = -1;
    //defines the size of a team -- do not adjust
    public const int TeamSize = 5;

    public const float Pi = 3.14159f;
    public const float TwoPi = Pi * 2;
    public const float HalfPi = Pi / 2;
    public const float QuarterPi = Pi / 4;

    public const int clockwise = 1;
    public const int anticlockwise = -1;
}

public enum region_modifier { halfsize, normal }

public class BC_MessageType
{
    public const int Msg_ReceiveBall =1;
    public const int Msg_PassToMe = 2;
    public const int Msg_SupportAttacker = 3;
    public const int Msg_GoHome = 4;
    public const int Msg_Wait = 5;
}

public enum team_color { blue, red };

public enum span_type { plane_backside, plane_front, on_plane };

public enum behavior_type
{
    none = 0,
    seek = 1,
    arrive = 2,
    separation = 4,
    pursuit = 8,
    interpose = 10
};