﻿using UnityEngine;
using System.Collections;
using System;

namespace BlackCoffee
{
    public class Utility
    {
        public static void Assert(bool success, string errors)
        {
            if (!success)
            {
                Debug.LogWarning(errors);
            }
        }

        internal static Vector2 Vector2Perp(Vector2 orgVector2)
        {
            return new Vector2(-orgVector2.y, orgVector2.x);
        }

        internal static void Assert(bool success)
        {
            if (!success)
                throw new Exception("Asset wrong");
        }

        internal static float Vec2DDistanceSq(Vector2 v1, Vector2 v2)
        {
            float ySeparation = v2.y - v1.y;
            float xSeparation = v2.x - v1.x;

            float distance = ySeparation * ySeparation + xSeparation * xSeparation;
            return distance;
        }

        internal static void Truncate(Vector2 v, float max)
        {
            if (v.magnitude > max)
            {
                v = v.normalized * max;
            }
        }

        internal static bool isZero(Vector2 v)
        {
            return v.sqrMagnitude < 0.0000001;
        }

        internal static bool GetTangentPoints(Vector2 C, float R, Vector2 P, out Vector2 T1, out Vector2 T2)
        {
            Vector2 PmC = P - C;
            float SqrLen = PmC.sqrMagnitude;
            float RSqr = R * R;
            if (SqrLen <= RSqr)
            {
                T1 = T2 = Vector2.zero;
                // P is inside or on the circle
                return false;
            }

            float InvSqrLen = 1 / SqrLen;
            float Root = Mathf.Sqrt(Mathf.Abs(SqrLen - RSqr));

            T1.x = C.x + R * (R * PmC.x - PmC.y * Root) * InvSqrLen;
            T1.y = C.y + R * (R * PmC.y + PmC.x * Root) * InvSqrLen;
            T2.x = C.x + R * (R * PmC.x + PmC.y * Root) * InvSqrLen;
            T2.y = C.y + R * (R * PmC.y - PmC.x * Root) * InvSqrLen;

            return true;
        }

        internal static Vector2 PointToLocalSpace(Vector2 point, Vector2 AgentHeading, Vector2 AgentSide, Vector2 AgentPosition)
        {
            //make a copy of the point
            Vector2 TransPoint = point;

            //create a transformation matrix
//             C2DMatrix matTransform = new C2DMatrix();
// 
//             float Tx = -Vector2.Dot(AgentPosition, AgentHeading);
//             float Ty = -Vector2.Dot(AgentPosition, AgentSide);
// 
//             //create the transformation matrix
//             matTransform._11(AgentHeading.x); matTransform._12(AgentSide.x);
//             matTransform._21(AgentHeading.y); matTransform._22(AgentSide.y);
//             matTransform._31(Tx); matTransform._32(Ty);
// 
//             //now transform the vertices
//             matTransform.TransformVector2Ds(ref TransPoint);

            return TransPoint;
        }

        /// <summary>
        /// 按照y轴旋转一定角度
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="angle"></param>
        internal static void RotateUp(ref Vector2 vec, float angle)
        {
            try
            {
                Vector3 vec3 = new Vector3(vec.x, 0, vec.y);
                Quaternion rotation = Quaternion.Euler(0, angle, 0);
                vec3 = rotation * vec3;

                vec.x = vec3.x;
                vec.y = vec3.z;
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex.ToString());
            }
        }

        //------------------------- WhereIsPoint --------------------------------------
        internal static span_type WhereIsPoint(Vector2 point,
                                      Vector2 PointOnPlane, //any point on the plane
                                      Vector2 PlaneNormal)
        {
            Vector2 dir = PointOnPlane - point;

            double d = Vector2.Dot(dir, PlaneNormal);

            if (d < -0.000001)
            {
                return span_type.plane_front;
            }

            else if (d > 0.000001)
            {
                return span_type.plane_backside;
            }

            return span_type.on_plane;
        }

        //given a plane and a ray this function determins how far along the ray 
        //an interestion occurs. Returns negative if the ray is parallel
        internal static float DistanceToRayPlaneIntersection(Vector2 RayOrigin,
                                             Vector2 RayHeading,
                                             Vector2 PlanePoint,  //any point on the plane
                                             Vector2 PlaneNormal)
        {
            float d = -Vector2.Dot(PlaneNormal, PlanePoint);
            float numer = Vector2.Dot(PlaneNormal, RayOrigin) + d;
            float denom = Vector2.Dot(PlaneNormal, RayHeading);

            // normal is parallel to vector
            if ((denom < 0.000001) && (denom > -0.000001))
            {
                return (-1.0f);
            }

            return -(numer / denom);
        }

        //--------------------LineIntersection2D-------------------------
        //
        //	Given 2 lines in 2D space AB, CD this returns true if an 
        //	intersection occurs.
        //
        //----------------------------------------------------------------- 

        internal static bool LineIntersection2D(Vector2 A,
                               Vector2 B,
                               Vector2 C,
                               Vector2 D)
        {
            double rTop = (A.y - C.y) * (D.x - C.x) - (A.x - C.x) * (D.y - C.y);
            double sTop = (A.y - C.y) * (B.x - A.x) - (A.x - C.x) * (B.y - A.y);

            double Bot = (B.x - A.x) * (D.y - C.y) - (B.y - A.y) * (D.x - C.x);

            if (Bot == 0)//parallel
            {
                return false;
            }

            double invBot = 1.0 / Bot;
            double r = rTop * invBot;
            double s = sTop * invBot;

            if ((r > 0) && (r < 1) && (s > 0) && (s < 1))
            {
                //lines intersect
                return true;
            }

            //lines do not intersect
            return false;
        }

        //--------------------------- Reflect ------------------------------------
        //
        //  given a normalized vector this method reflects the vector it
        //  is operating upon. (like the path of a ball bouncing off a wall)
        //------------------------------------------------------------------------
        internal static void Reflect(ref Vector2 vec, Vector2 norm)
        {
            vec += 2.0f * Vector2.Dot(vec, norm) * GetReverse(norm);
        }

        //----------------------- GetReverse ----------------------------------------
        //
        //  returns the vector that is the reverse of this vector
        //------------------------------------------------------------------------
        internal static Vector2 GetReverse(Vector2 vec)
        {
            return new Vector2(-vec.x, -vec.y);
        }

        internal static float RandFloat()
        {
            return UnityEngine.Random.Range(0.0f, 1.0f);
        }

        internal static float RandInRange(float x, float y)
        {
            return x + RandFloat() * (y - x);
        }

        //returns a random double in the range -1 < n < 1
        internal static float RandomClamped() { return RandFloat() - RandFloat(); }

        //----------------------------- AddNoiseToKick --------------------------------
        //
        //  this can be used to vary the accuracy of a player's kick. Just call it 
        //  prior to kicking the ball using the ball's position and the ball target as
        //  parameters.
        //-----------------------------------------------------------------------------
        internal static Vector2 AddNoiseToKick(Vector2 BallPos, Vector2 BallTarget)
        {

            float displacement = (Mathf.PI - Mathf.PI * Prm.PlayerKickingAccuracy) * RandomClamped();

            Vector2 toTarget = BallTarget - BallPos;

            Utility.RotateUp(ref toTarget, displacement);

            return toTarget + BallPos;
        }

        //------------------------ Sign ------------------------------------------
        //
        //  returns positive if v2 is clockwise of this vector,
        //  minus if anticlockwise (Y axis pointing down, X axis to right)
        //------------------------------------------------------------------------
        internal static int Sign(Vector2 v1, Vector2 v2)
        {
            if (v1.y * v2.x > v1.x * v2.y)
            {
                return Common.anticlockwise;
            }
            else
            {
                return Common.clockwise;
            }
        }

        internal static bool IsEqual(float num1, float num2)
        {
            return Mathf.Abs(num1 - num1) < float.Epsilon;
        }
    }
}