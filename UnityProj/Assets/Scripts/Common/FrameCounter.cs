﻿using UnityEngine;
using System.Collections;

namespace BlackCoffee
{
    public class FrameCounter
    {
        internal static readonly FrameCounter Instance = new FrameCounter();

        long m_lCount;

        int m_iFramesElapsed;

        FrameCounter()
        {
            m_lCount = 0; m_iFramesElapsed = 0;
        }

        //copy ctor and assignment should be private
        //FrameCounter(const FrameCounter&);
        //FrameCounter& operator=(const FrameCounter&);

        //////////////////////////////////////////////////////////////////////////public

        //static FrameCounter* Instance();

        internal void Update() { ++m_lCount; ++m_iFramesElapsed; }

        internal double GetCurrentFrame(){return m_lCount;}

        internal void Reset() { m_lCount = 0; }

        internal void Start() { m_iFramesElapsed = 0; }
        internal int FramesElapsedSinceStartCalled() { return m_iFramesElapsed; }
    }
}
