﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BlackCoffee
{
    internal class SteeringBehaviors
    {
        //////////////////////////////////////////////////////////////////////////private
        MovingEntity m_pPlayer;
        MovingEntity m_pTarget;

        //the steering force created by the combined effect of all
        //the selected behaviors
        Vector2 m_vSteeringForce;

        //the current target (usually the ball or predicted ball position)
        Vector2 m_vTarget;

        //the distance the player tries to interpose from the target
        float m_dInterposeDist;

        //multipliers. 
        float m_dMultSeparation;

        //how far it can 'see'
        float m_dViewDistance;


        //binary flags to indicate whether or not a behavior should be active
        behavior_type m_iFlags;

        //used by group behaviors to tag neighbours
        bool m_bTagged;

        //Arrive makes use of these to determine how quickly a vehicle
        //should decelerate to its target
        enum Deceleration { slow = 3, normal = 2, fast = 1 };

        //------------------------------- Seek -----------------------------------
        //
        //  Given a target, this behavior returns a steering force which will
        //  allign the agent with the target and move the agent in the desired
        //  direction
        //------------------------------------------------------------------------
        //this behavior moves the agent towards a target position
        Vector2 Seek(Vector2 target)
        {
            Vector2 DesiredVelocity = (target - m_pPlayer.Pos()).normalized
                                      * m_pPlayer.MaxSpeed();

            return (DesiredVelocity - m_pPlayer.Velocity());
        }

        //this behavior is similar to seek but it attempts to arrive 
        //at the target with a zero velocity
        Vector2 Arrive(Vector2 target, Deceleration decel)
        {
            Vector2 ToTarget = target - m_pPlayer.Pos();

            //calculate the distance to the target
            float dist = ToTarget.magnitude;

            if (dist > 0)
            {
                //because Deceleration is enumerated as an int, this value is required
                //to provide fine tweaking of the deceleration..
                const float DecelerationTweaker = 0.3f;

                //calculate the speed required to reach the target given the desired
                //deceleration
                float speed = dist / ((float)decel * DecelerationTweaker);

                //make sure the velocity does not exceed the max
                speed = Mathf.Min(speed, m_pPlayer.MaxSpeed());

                //from here proceed just like Seek except we don't need to normalize 
                //the ToTarget vector because we have already gone to the trouble
                //of calculating its length: dist. 
                Vector2 DesiredVelocity = ToTarget * speed / dist;

                return (DesiredVelocity - m_pPlayer.Velocity());
            }

            return new Vector2(0, 0);
        }

        //This behavior predicts where its prey will be and seeks
        //to that location
        Vector2 Pursuit(MovingEntity target)
        {
            //首先计算朝向
            float RelativeHeading = Vector2.Dot(m_pPlayer.Heading(), target.Heading());

            Vector2 ToTar = target.Pos() - m_pPlayer.Pos();
            //如果被追逐对象面向追逐着，则直接调用seek
            if ((Vector2.Dot(ToTar, m_pPlayer.Heading()) > 0) &&
                    (RelativeHeading < -0.95))  //acos(0.95)=18 degs
            {
                return Seek(target.Pos());
            }

            //the lookahead time is proportional to the distance between the ball
            //and the pursuer; 
            float LookAheadTime = 0.0f;

            if (target.Speed() != 0.0)
            {
                LookAheadTime = ToTar.magnitude / target.Speed();
            }

            //calculate where the ball will be at this time in the future
            m_vTarget = target.Pos() + target.Velocity() * LookAheadTime;

            //now seek to the predicted future position of the ball
            return Arrive(m_vTarget, Deceleration.fast);
        }

        //---------------------------- Separation --------------------------------
        //
        // this calculates a force repelling from the other neighbors
        //------------------------------------------------------------------------
        Vector2 Separation()
        {
            //iterate through all the neighbors and calculate the vector from the
            Vector2 SteeringForce = Vector2.zero;

            foreach (TestBehavior curPlyr in TestBehavior.AllMembers)
            {
                //make sure this agent isn't included in the calculations and that
                //the agent is close enough
                if ((curPlyr != m_pPlayer) && curPlyr.Steering().Tagged())
                {
                    Vector2 ToAgent = m_pPlayer.Pos() - curPlyr.Pos();

                    //scale the force inversely proportional to the agents distance  
                    //from its neighbor.
                    SteeringForce += ToAgent.normalized / ToAgent.magnitude;
                }
            }

            return SteeringForce;
        }

        //this attempts to steer the agent to a position between the opponent
        //and the object
        Vector2 Interpose(MovingEntity ball,
                           Vector2 target,
                           float DistFromTarget)
        {
            return Arrive(target + (ball.Pos() - target).normalized *
                          DistFromTarget, Deceleration.normal);
        }


        //finds any neighbours within the view radius
        void FindNeighbours()
        {
            foreach (TestBehavior curPlyr in TestBehavior.AllMembers)
            {
                //first clear any current tag
                curPlyr.Steering().UnTag();

                //work in distance squared to avoid sqrts
                Vector2 to = curPlyr.Pos() - m_pPlayer.Pos();

                if (to.sqrMagnitude < (m_dViewDistance * m_dViewDistance))
                {
                    curPlyr.Steering().Tag();
                }
            }//next
        }


        //--------------------- AccumulateForce ----------------------------------
        //
        //  This function calculates how much of its max steering force the 
        //  vehicle has left to apply and then applies that amount of the
        //  force to add.
        //------------------------------------------------------------------------
        bool AccumulateForce(ref Vector2 sf, Vector2 ForceToAdd)
        {
            //first calculate how much steering force we have left to use
            float MagnitudeSoFar = sf.magnitude;

            float magnitudeRemaining = m_pPlayer.MaxForce() - MagnitudeSoFar;

            //return false if there is no more force left to use
            if (magnitudeRemaining <= 0.0) return false;

            //calculate the magnitude of the force we want to add
            float MagnitudeToAdd = ForceToAdd.magnitude;

            //now calculate how much of the force we can really add  
            if (MagnitudeToAdd > magnitudeRemaining)
            {
                MagnitudeToAdd = magnitudeRemaining;
            }

            //add it to the steering force
            sf += ForceToAdd.normalized * MagnitudeToAdd;

            return true;
        }

        //-------------------------- SumForces -----------------------------------
        //
        //  this method calls each active steering behavior and acumulates their
        //  forces until the max steering force magnitude is reached at which
        //  time the function returns the steering force accumulated to that 
        //  point
        //------------------------------------------------------------------------
        Vector2 SumForces()
        {
            Vector2 force = Vector2.zero;

            //the soccer players must always tag their neighbors
            FindNeighbours();

            if (On(behavior_type.separation))
            {
                force += Separation() * m_dMultSeparation;

                if (!AccumulateForce(ref m_vSteeringForce, force)) return m_vSteeringForce;
            }

            if (On(behavior_type.seek))
            {
                force += Seek(m_vTarget);

                if (!AccumulateForce(ref m_vSteeringForce, force)) return m_vSteeringForce;
            }

            if (On(behavior_type.arrive))
            {
                force += Arrive(m_vTarget, Deceleration.slow);

                if (!AccumulateForce(ref m_vSteeringForce, force)) return m_vSteeringForce;
            }

            if (On(behavior_type.pursuit))
            {
                force += Pursuit(m_pTarget);

                if (!AccumulateForce(ref m_vSteeringForce, force)) return m_vSteeringForce;
            }

            if (On(behavior_type.interpose))
            {
                force += Interpose(m_pTarget, m_vTarget, m_dInterposeDist);

                if (!AccumulateForce(ref m_vSteeringForce, force)) return m_vSteeringForce;
            }

            return m_vSteeringForce;
        }

        //a vertex buffer to contain the feelers rqd for dribbling
        List<Vector2> m_Antenna;

        //////////////////////////////////////////////////////////////////////////public
        internal SteeringBehaviors(MovingEntity agent)
        {
            m_pPlayer=agent;
             m_iFlags=0;
             m_dMultSeparation=Prm.SeparationCoefficient;
             m_bTagged=false;
             m_dViewDistance=Prm.ViewDistance;
             m_dInterposeDist=0.0f;
             m_Antenna = new List<Vector2>(5);
        }

        //virtual ~SteeringBehaviors(){}

        //---------------------- Calculate ---------------------------------------
        //
        //  calculates the overall steering force based on the currently active
        //  steering behaviors. 
        //------------------------------------------------------------------------
        internal Vector2 Calculate()
        {
            //reset the force
            m_vSteeringForce = Vector2.zero;

            //this will hold the value of each individual steering force
            m_vSteeringForce = SumForces();

            //make sure the force doesn't exceed the vehicles maximum allowable
            Utility.Truncate(m_vSteeringForce, m_pPlayer.MaxForce());

            return m_vSteeringForce;
        }

        //calculates the component of the steering force that is parallel
        //with the vehicle heading
        internal float ForwardComponent()
        {
            float dotRes = Vector2.Dot(m_pPlayer.Heading(), m_vSteeringForce);
            return dotRes;
        }

        //calculates the component of the steering force that is perpendicuar
        //with the vehicle heading
        internal float SideComponent()
        {
            float dotRes = Vector2.Dot(m_pPlayer.Side(), m_vSteeringForce);
            dotRes = -Utility.Sign(m_pPlayer.Heading(), m_vSteeringForce) * Mathf.Abs(dotRes);
            return dotRes * m_pPlayer.MaxTurnRate();
        }

        internal Vector2 Force() { return m_vSteeringForce; }

        //renders visual aids and info for seeing how each behavior is
        //calculated
        //internal void RenderInfo();
        //internal void RenderAids();

        internal Vector2 Target() { return m_vTarget; }
        internal void SetTarget(Vector2 t) { m_vTarget = t; }

        internal double InterposeDistance() { return m_dInterposeDist; }
        internal void SetInterposeDistance(float d) { m_dInterposeDist = d; }

        internal bool Tagged() { return m_bTagged; }
        internal void Tag() { m_bTagged = true; }
        internal void UnTag() { m_bTagged = false; }

        //this function tests if a specific bit of m_iFlags is set
        bool On(behavior_type bt) { return (m_iFlags & bt) == bt; }

        internal void SeekOn() { m_iFlags |= behavior_type.seek; }
        internal void ArriveOn() { m_iFlags |= behavior_type.arrive; }
        internal void PursuitOn() { m_iFlags |= behavior_type.pursuit; }
        internal void SeparationOn() { m_iFlags |= behavior_type.separation; }
        internal void InterposeOn(float d) { m_iFlags |= behavior_type.interpose; m_dInterposeDist = d; }


        internal void SeekOff() { if (On(behavior_type.seek))   m_iFlags &= ~behavior_type.seek; }
        internal void ArriveOff() { if (On(behavior_type.arrive)) m_iFlags &= ~behavior_type.arrive; }
        internal void PursuitOff() { if (On(behavior_type.pursuit)) m_iFlags &= ~behavior_type.pursuit; }
        internal void SeparationOff() { if (On(behavior_type.separation)) m_iFlags &= ~behavior_type.separation; }
        internal void InterposeOff() { if (On(behavior_type.interpose)) m_iFlags &= ~behavior_type.interpose; }


        internal bool SeekIsOn() { return On(behavior_type.seek); }
        internal bool ArriveIsOn() { return On(behavior_type.arrive); }
        internal bool PursuitIsOn() { return On(behavior_type.pursuit); }
        internal bool SeparationIsOn() { return On(behavior_type.separation); }
        internal bool InterposeIsOn() { return On(behavior_type.interpose); }
    }
}
